#!/bin/bash

mainOptions=("Operating System Info" "Hardware List" "Free and Used Memory" "Hardware Detail" "Exit")
hardwareOptions=("CPU" "Block Devices" "Back to Previous Menu")


pause_shell() {
    read -p "Press [Enter] key to continue..."
}

show_system_status() {
    echo "-----------------------------"
    echo "        SYSTEM STATUS        "
    echo "-----------------------------"
    echo "Username: "${USER}
    echo "OS: "$(uname) $(uname -r)
    echo -E "Uptime: " $(uptime -p | awk '{for (i = 2; i <= NF; i++) print $i}')"," $(uptime | awk '{print $4}') "user"
    echo "IP: "$(hostname -I)
    echo "Hostname: "$(hostname)
    pause_shell
}

show_hardware_list() {
    echo "-----------------------------"
    echo "        HARDWARE LIST        "
    echo "-----------------------------"
    echo "Machine Hardware: "$(lscpu | grep Architecture | awk '{print $2}')
    echo "$(lshw -short 2> /dev/null)"
    pause_shell
}

show_memory_detail() {
    echo "-----------------------------"
    echo "           MEMORY            "
    echo "-----------------------------"
    echo "********"
    echo " Memory "
    echo "********"
    echo "Size: "$(vmstat -s | grep 'total memory' | awk {'print $1'})
    echo "Free: "$(vmstat -s | grep 'free memory' | awk {'print $1'})
    echo "*******************"
    echo " Memory Statistics "
    echo "*******************"
    echo $(vmstat)
    echo "******************"
    echo " Top 10 CPU Usage "
    echo "******************"
    echo "$(ps aux --sort=-%cpu | head --lines=11)"
    pause_shell
}

show_cpu_info() {
    echo "**********"
    echo " CPU Info "
    echo "**********"
    echo "$(lscpu | grep -v "Flags")"
    pause_shell
}

show_device_block_info() {
    echo "***************"
    echo " Block Devices "
    echo "***************"
    echo "$(lsblk)"
    pause_shell
}

while true
do
    PS3="Choose 1 - ${#mainOptions[@]}: "
    echo "=============================="
    echo "          MAIN MENU           "
    echo "=============================="
    select option in "${mainOptions[@]}"
    do
        case $option in
            "${mainOptions[0]}") 
                show_system_status
                break
            ;;
            "${mainOptions[1]}") 
                show_hardware_list
                break
            ;;
            "${mainOptions[2]}") 
                show_memory_detail
                break
            ;;
            "${mainOptions[3]}")
                while true
                do
                    PS3="Choose 1 - ${#hardwareOptions[@]}: "
                    echo "=============================="
                    echo "       HARDWARE DETAIL        "
                    echo "=============================="
                    select option in "${hardwareOptions[@]}"
                    do
                        case $option in
                            "${hardwareOptions[0]}") 
                                show_cpu_info
                                break
                            ;;
                            "${hardwareOptions[1]}") 
                                show_device_block_info
                                break
                            ;;
                            "${hardwareOptions[2]}")
                                echo "returning to main menu..."
                                break 3
                            ;;
                        esac
                    done
                done
            ;;
            "${mainOptions[4]}")
                echo "Fare ye well..."
                break 2
            ;;
        esac 
    done
done